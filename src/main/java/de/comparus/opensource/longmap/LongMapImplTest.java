package de.comparus.opensource.longmap;

import java.util.HashMap;

import static org.junit.Assert.*;

public class LongMapImplTest {

    LongMapImpl<String> actual = new LongMapImpl<>();
    HashMap<Long,String> expected = new HashMap<>();

    @org.junit.Before
    public void setUp() throws Exception {
        for (long i = 0; i < 20; i++){
            String value = "test "+i;
            actual.put(i, value);
            expected.put(i, value);
        }
    }

    @org.junit.After
    public void tearDown() throws Exception {
        actual.clear();
    }

    @org.junit.Test
    public void put() {
        actual.put((long)123,"test put method");
        expected.put((long)123,"test put method");
        assertEquals(expected.size(), actual.size());
    }

    @org.junit.Test
    public void get() {
        assertEquals(expected.get(123), actual.get(123));
    }

    @org.junit.Test
    public void remove() {
        actual.remove(123);
        expected.remove(123);
        assertEquals(expected.get(123), actual.get(123));
    }

    @org.junit.Test
    public void isEmpty() {
        assertEquals(expected.isEmpty(), actual.isEmpty());
    }

    @org.junit.Test
    public void containsKey() {
        assertEquals(expected.containsKey(123), actual.containsKey(123));
    }

    @org.junit.Test
    public void containsValue() {
        assertEquals(expected.containsValue("test 1"), actual.containsValue("test 1"));
    }
    @org.junit.Test
    public void size() {
        assertEquals(expected.size(),actual.size());
    }

    @org.junit.Test
    public void clear() {
        assertEquals(expected.size(),actual.size());
        expected.clear();
        actual.clear();
        assertEquals(expected.size(),actual.size());
    }
}