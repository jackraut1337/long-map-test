package de.comparus.opensource.longmap;

public class Entry<V>
{
    int hash;
    long key;
    V value;
    Entry next;

    Entry(int hash, long key, V value, Entry<V> next)
    {
        this.hash = hash;
        this.key = key;
        this.value = value;
        this.next = next;
    }
}
