package de.comparus.opensource.longmap;
import java.util.Arrays;

public class LongMapImpl<V> implements LongMap<V>
{
    private final float DEFAULT_LOAD_FACTOR = 4;
    private int length = 16, actualSize =0;
    private Entry[] entries = new Entry[length];


    public V put(long key, V value)
    {
        if((Long)key == null) return putForNullKey(value);
        if (actualSize / length > DEFAULT_LOAD_FACTOR) resize(2* length);

        int hash = Long.hashCode(key);
        int i = indexFor(key);
        if (entries[i] == null)
        {
            entries[i] = new Entry<V>(hash, key, value, null);
            actualSize++;
            return null;
        }
        for (Entry<V> e = entries[i]; e != null; e = e.next)
        {
            if (e.hash == hash && e.key == key)
            {
                V oldValue = e.value;
                e.value = value;
                return oldValue;
            }
            if (e.next == null){
                e.next = new Entry<V>(hash, key, value, null);
                actualSize++;
                break;
            }
        }
        return null;
    }

    private V putForNullKey(V value) {
        for (Entry<V> e = entries[0]; e != null; e = e.next)
        {
            if ((Long)e.key == null)
            {
                V oldValue = e.value;
                e.value = value;
                return oldValue;
            }
            if (e.next == null){
                e.next = new Entry<V>(0, (Long)null, value, null);
                actualSize++;
                break;
            }
        }
        return null;
    }

    public V get(long key)
    {
        int i = indexFor(key);
        for (Entry<V> e = entries[i]; e != null; e = e.next)
        {
            if (e.key == key)
            {
                return e.value;
            }
        }
        return null;
    }

    public V remove(long key)
    {
        int i = indexFor(key);
        Entry<V> e = entries[i];
        Entry<V> last = null;
        while (e != null)
        {
            if (key == e.key)
            {
                if (last == null)
                    entries[i] = e.next;
                else
                    last.next = e.next;
                actualSize--;
                return e.value;
            }
            last = e;
            e = e.next;
        }
        return null;
    }

    public boolean containsKey(long key)
    {
        int i = indexFor(key);
        for (Entry e = entries[i]; e != null; e = e.next)
        {
            if (e.key == key)
            {
                return true;
            }
        }
        return false;
    }

    public boolean containsValue(V value)
    {
        for(Entry e : entries)
        {
            while (e != null)
            {
                if (e.value.equals(value))
                    return true;
                e = e.next;
            }
        }
        return false;
    }

    public long[] keys()
    {
        long[] array = new long[actualSize];
        int i = 0;
        for(Entry e : entries)
            while (e != null)
            {
                array[i++] = e.key;
                e = e.next;
            }
        return array;
    }

    public V[] values()
    {
        V[] array = (V[]) new Object[actualSize];
        int i = 0;
        for(Entry<V> e : entries)
            while (e != null)
            {
                array[i++] = e.value;
                e = e.next;
            }
        return array;
    }



    public void clear()
    {
        if(actualSize > 0)
        {
            Arrays.fill(entries, null);
            actualSize = 0;
        }
    }

    private void resize(int newCapacity)
    {
        Entry[] newTable = new Entry[newCapacity];
        transfer(newTable);
        entries = newTable;
    }

    private void transfer(Entry[] newTable)
    {
        Entry[] src = entries;
        for (int j = 0; j < src.length; j++) {
            Entry<V> e = src[j];
            if (e != null) {
                src[j] = null;
                do {
                    Entry<V> next = e.next;
                    int i = indexFor(e.key);
                    e.next = newTable[i];
                    newTable[i] = e;
                    e = next;
                } while (e != null);
            }
        }
    }

    public boolean isEmpty() { return actualSize == 0; }

    public long size() { return actualSize; }

    private int indexFor(long key) { return Long.hashCode(key) & (length -1); }


}
